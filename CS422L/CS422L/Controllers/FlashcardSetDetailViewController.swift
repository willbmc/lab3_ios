//
//  FlashcardSetDetailViewController.swift
//  CS422L
//
//  Created by Student Account  on 2/3/22.
//

import UIKit

class FlashcardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    

        @IBOutlet var tableView: UITableView!
    
    
        
        var set: [Flashcard] = Flashcard.getHardCodedFlashcards()
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            let setClass = Flashcard()
            //connect hard coded collection to sets
            tableView.delegate = self
            tableView.dataSource = self
        }

        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            //return number of items
            return set.count
        }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell  {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SetCell1", for: indexPath) as! FlashcardCollectionCellTableViewCell
            
            cell.label1.text = set[indexPath.row].term
        cell.label2.text = set[indexPath.row].definition
            cell.backgroundColor = UIColor.purple
            
            return cell
        }
        
        
    @IBAction func add2btn(_ sender: Any) {
        let newSet = Flashcard()
        newSet.term = "new Term"
        newSet.definition = "New Def"
        set.append(newSet)
        tableView.reloadData()
    }
    
        
    
}
