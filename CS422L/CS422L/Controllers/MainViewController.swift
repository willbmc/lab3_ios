//
//  ViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 1/29/21.
//

import UIKit

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {

    
    @IBOutlet var collectionView: UICollectionView!
    
    
    var sets: [FlashcardSet] = FlashcardSet.getHardCodedCollection()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let setClass = FlashcardSet()
        //connect hard coded collection to sets
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return number of items
        return sets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetCell", for: indexPath) as! FlashcardSetCollectionCell
        
        cell.myLable.text = sets[indexPath.row].title
        cell.backgroundColor = UIColor.purple
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //go to new view
        performSegue(withIdentifier: "GoToDetail", sender: self)
        
    }
    
    @IBAction func add1btn(_ sender: Any) {
        
        let newSet = FlashcardSet()
        newSet.title = "new Set"
        sets.append(newSet)
        collectionView.reloadData()
        
    }
}

